<?php

namespace App\Http\Controllers;

use App\Http\Requests\TalkToUsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class RDController extends Controller
{

    public function talkToUs(TalkToUsRequest $req)
    {
        $accessToken = $req->access_token;
        $headers = ["Authorization" => "Bearer $accessToken"];

        $user = [
            "name" => $req->name,
            "mobile_phone" => $req->phone,
            "cf_cidades_voa" => $req->city,
            "cf_assunto" => $req->subject,
            "cf_mensagem" => $req->message,
            "tags" => [
                "fale_conosco"
            ]
        ];

        $checkUser = Http::withHeaders($headers)
            ->get("https://api.rd.services/platform/contacts/email:$req->email");

        if ($checkUser->status() == 200) {
            $patchUser = Http::withHeaders($headers)->patch("https://api.rd.services/platform/contacts/email:$req->email", $user);

            if ($patchUser->status() == 200) {

                $patchUser = json_decode($patchUser->body());
                $patchUser->links = null;
                $patchUser->uuid = null;

                return response([
                    "message" => "User successfully updated.",
                    "user" => $patchUser,
                    "success" => true
                ], 200);
            }

            return response([
                "Something went wrong while updating user",
                "data" => $patchUser->body()
            ], 400);
        }

        $user['email'] = $req->email;

        $addUser = Http::withHeaders($headers)->post("https://api.rd.services/platform/contacts", $user);
        if ($addUser->status() == 200) {
            $addUser = json_decode($addUser->body());
            $addUser->links = null;
            $addUser->uuid = null;

            return response([
                "message" => "User successfully created.",
                "user" => $addUser,
                "success" => true
            ], 200);
        }

        return response([
            "Something went wrong while creating a user",
            "data" => $addUser->body()
        ], 400);
    }

    public function sendResume(Request $req)
    {
        $accessToken = $req->access_token;
        $headers = ["Authorization" => "Bearer $accessToken"];

        $user = [
            "name" => $req->name,
            "mobile_phone" => $req->phone,
            "cf_cidades_voa" => $req->city,
            "cf_area_trabalho" => $req->occupation_area,
            "cf_resume_link" => $req->resume_link,
            "cf_mensagem" => $req->message,
            "tags" => [
                "enviou_curriculo"
            ]
        ];

        $checkUser = Http::withHeaders($headers)
            ->get("https://api.rd.services/platform/contacts/email:$req->email");

        if ($checkUser->status() == 200) {
            $patchUser = Http::withHeaders($headers)->patch("https://api.rd.services/platform/contacts/email:$req->email", $user);

            if ($patchUser->status() == 200) {

                $patchUser = json_decode($patchUser->body());
                $patchUser->links = null;
                $patchUser->uuid = null;

                return response([
                    "message" => "User successfully updated.",
                    "user" => $patchUser,
                    "success" => true
                ], 200);
            }

            return response([
                "Something went wrong while updating user",
                "data" => $patchUser->body()
            ], 400);
        }

        $user['email'] = $req->email;

        $addUser = Http::withHeaders($headers)->post("https://api.rd.services/platform/contacts", $user);
        if ($addUser->status() == 200) {
            $addUser = json_decode($addUser->body());
            $addUser->links = null;
            $addUser->uuid = null;

            return response([
                "message" => "User successfully created.",
                "user" => $addUser,
                "success" => true
            ], 200);
        }

        return response([
            "Something went wrong while creating a user",
            "data" => $addUser->body()
        ], 400);
    }

    public function hireAPlan(Request $req)
    {
        $accessToken = $req->access_token;
        $headers = ["Authorization" => "Bearer $accessToken"];

        $user = [
            "name" => $req->name,
            "mobile_phone" => $req->phone,
            "cf_cidades_voa" => $req->city,
            "state" => $req->state,
            "cf_documento" => $req->document,
            "cf_cep" => (int)$req->zipcode,
            "cf_bairro" => $req->neighborhood,
            "cf_rua" => $req->address,
            "cf_numero" => $req->number,
            "cf_complemento" => $req->complement,
            "cf_televisao" => $req->television ? 'SIM' : 'NÃO',
            "cf_internet_movel" => $req->mobile ? 'SIM' : 'NÃO',
            "cf_canais_pagos" => $req->paid_channels ? 'SIM' : 'NÃO',
            "tags" => [
                "contratar_plano_" . mb_strtolower($req->plan, 'utf-8')
            ]
        ];



        $checkUser = Http::withHeaders($headers)
            ->get("https://api.rd.services/platform/contacts/email:$req->email");

        if ($checkUser->status() == 200) {
            $patchUser = Http::withHeaders($headers)->patch("https://api.rd.services/platform/contacts/email:$req->email", $user);

            if ($patchUser->status() == 200) {

                $patchUser = json_decode($patchUser->body());
                $patchUser->links = null;
                $patchUser->uuid = null;

                return response([
                    "message" => "User successfully updated.",
                    "user" => $patchUser,
                    "success" => true
                ], 200);
            }

            return response([
                "Something went wrong while updating user",
                "data" => $patchUser->body()
            ], 400);
        }

        $user['email'] = $req->email;

        $addUser = Http::withHeaders($headers)->post("https://api.rd.services/platform/contacts", $user);
        if ($addUser->status() == 200) {
            $addUser = json_decode($addUser->body());
            $addUser->links = null;
            $addUser->uuid = null;

            return response([
                "message" => "User successfully created.",
                "user" => $addUser,
                "success" => true
            ], 200);
        }

        return response([
            "Something went wrong while creating a user",
            "data" => $addUser->body()
        ], 400);
    }

    public function notifyMe(Request $req)
    {
        $accessToken = $req->access_token;
        $headers = ["Authorization" => "Bearer $accessToken"];

        $user = [
            "cf_cidade_sem_cobertura" => $req->city,
            "tags" => [
                "avise_me"
            ]
        ];

        $checkUser = Http::withHeaders($headers)
            ->get("https://api.rd.services/platform/contacts/email:$req->email");

        if ($checkUser->status() == 200) {
            $patchUser = Http::withHeaders($headers)->patch("https://api.rd.services/platform/contacts/email:$req->email", $user);

            if ($patchUser->status() == 200) {

                $patchUser = json_decode($patchUser->body());
                $patchUser->links = null;
                $patchUser->uuid = null;

                return response([
                    "message" => "User successfully updated.",
                    "user" => $patchUser,
                    "success" => true
                ], 200);
            }

            return response([
                "Something went wrong while updating user",
                "data" => $patchUser->body()
            ], 400);
        }

        $user['email'] = $req->email;

        $addUser = Http::withHeaders($headers)->post("https://api.rd.services/platform/contacts", $user);
        if ($addUser->status() == 200) {
            $addUser = json_decode($addUser->body());
            $addUser->links = null;
            $addUser->uuid = null;

            return response([
                "message" => "User successfully created.",
                "user" => $addUser,
                "success" => true
            ], 200);
        }

        return response([
            "Something went wrong while creating a user",
            "data" => $addUser->body()
        ], 400);
    }

    public function uploadCv(Request $req)
    {
        $req->validate([
            'curriculum' => 'required|mimes:pdf,doc,docx,jpeg,png,jpg,gif,svg|max:4096',
        ]);

        $imageName = time() . '.' . $req->curriculum->extension();

        $path = Storage::disk('s3')->put('curriculums', $req->curriculum);
        $path = Storage::url($path);


        return response([
            "message" => "File uploaded successfully",
            "url" => "https://mundovoa-files.s3.amazonaws.com/" . explode('/storage/', $path)[1]
        ]);
    }
}
