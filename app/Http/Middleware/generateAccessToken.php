<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class generateAccessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $res = Http::post('https://api.rd.services/auth/token', [
            "client_id" => "91baf25a-6e4f-490b-8c96-da86ab066c24",
            "client_secret" => "aa320197c91d4b90ba10b6a8aea57229",
            "refresh_token" => "UGquPDH5RGE8tzhyMngWwVQeJ2_RlwCF9QWuYjQVoxc"
        ]);

        ;

        $request_array = $request->all();
        $request_array['access_token'] = json_decode($res->body())->access_token;
        $request->replace($request_array);
        return $next($request);
    }
}
