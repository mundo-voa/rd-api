<?php

use App\Http\Controllers\RDController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/talktous', [RDController::class, 'talkToUs'])->middleware('generateAccessToken');
Route::post('/sendresume', [RDController::class, 'sendResume'])->middleware('generateAccessToken');
Route::post('/hireaplan', [RDController::class, 'hireAPlan'])->middleware('generateAccessToken');
Route::post('/notifyme', [RDController::class, 'notifyMe'])->middleware('generateAccessToken');
Route::post('/upload', [RDController::class, 'uploadCv'])->middleware('generateAccessToken');;
